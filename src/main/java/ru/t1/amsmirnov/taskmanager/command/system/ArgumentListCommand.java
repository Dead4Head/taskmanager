package ru.t1.amsmirnov.taskmanager.command.system;

import ru.t1.amsmirnov.taskmanager.api.model.ICommand;
import ru.t1.amsmirnov.taskmanager.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentListCommand extends AbstractSystemCommand {

    public static final String NAME = "arguments";
    public static final String DESCRIPTION = "Show available arguments.";
    public static final String ARGUMENT = "-args";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            if (command == null) continue;
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
