# TASK MANAGER

## DEVELOPER INFO

**NAME**: Anton Smirnov

**EMAIL**: amsmirnov@t1-consulting.ru

**EMAIL**: smiru2000@gmail.com

## SOFTWARE 

**JAVA**: 1.8 OPENJDK

**OS**: WINDOWS 10 x64

## HARDWARE

**CPU**: i5-1135G7

**RAM**: 16 Gb

**SSD**: 256

## BUILD PROGRAM

```
mvn clean install
```

## RUN PROGRAM

```
java -jar ./Taskmanager.jar
```

