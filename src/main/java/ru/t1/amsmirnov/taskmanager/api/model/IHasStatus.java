package ru.t1.amsmirnov.taskmanager.api.model;

import ru.t1.amsmirnov.taskmanager.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
