package ru.t1.amsmirnov.taskmanager.exception.user;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public final class PermissionDeniedException extends AbstractException {

    public PermissionDeniedException() {
        super("Error! Permission is incorrect...");
    }

}
